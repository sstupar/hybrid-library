import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BookModel} from '../../models/book.model';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  imagePath;
  constructor(private _sanitizer: DomSanitizer) {
  }

  @Input() book: BookModel;
  @Output() rent: EventEmitter<any> = new EventEmitter();
  @Output() edit: EventEmitter<any> = new EventEmitter();
  @Output() delete: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.imagePath = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
      + this.book.coverImageHash);
  }

  rentBook() {
    this.rent.emit(this.book.id);
  }

  editBook() {
    this.edit.emit(this.book.id);
  }

  deleteBook() {
    this.delete.emit(this.book.id);
  }

}
