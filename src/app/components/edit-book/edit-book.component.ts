import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {BookModel} from '../../models/book.model';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.scss']
})
export class EditBookComponent implements OnInit, OnChanges {
  @Input() book: BookModel;
  @Output() update: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();
  @Output() changeImage: EventEmitter<any> = new EventEmitter();
  editForm: FormGroup;
  imagePath;
  imageFile;

  constructor(private _sanitizer: DomSanitizer) {
  }

  ngOnInit() {
    this.editForm = new FormGroup({
      author: new FormControl(this.book.author),
      publisher: new FormControl(this.book.publisher),
      title: new FormControl(this.book.title),
      numberOfCopies: new FormControl(this.book.numberOfCopies),
      numberOfAvailableCopies: new FormControl(this.book.numberOfAvailableCopies),
      isbn: new FormControl(this.book.isbn),
      id: new FormControl(this.book.id)
    });
    this.imagePath = this.createImagePath();
  }

  onSubmit() {
    this.update.emit(this.editForm.value);
  }

  ngOnChanges(changes) {
    if (!changes.book.firstChange &&
      changes.book.currentValue.coverImageHash !== changes.book.previousValue.coverImageHash) {
      this.book.coverImageHash = changes.book.currentValue.coverImageHash;
    }
    this.imagePath = this.createImagePath();
  }

  createImagePath() {
    return this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
      + this.book.coverImageHash);
  }

  closeForm() {
    this.close.emit();
  }

  handleFileInput(file) {
    this.imageFile = file;
  }

  updateImage() {
    this.changeImage.emit({image: this.imageFile, id: this.editForm.value.id});
  }

}
