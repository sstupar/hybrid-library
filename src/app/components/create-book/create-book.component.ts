import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.scss']
})
export class CreateBookComponent implements OnInit {
  @Output() create: EventEmitter<any> = new EventEmitter();
  @Output() close: EventEmitter<any> = new EventEmitter();
  createForm: FormGroup;
  imageFile;
  constructor() {
    this.createForm = new FormGroup({
      author: new FormControl(null, Validators.required),
      publisher: new FormControl(null, Validators.required),
      title: new FormControl(null, Validators.required),
      numberOfCopies: new FormControl(null, Validators.required),
      numberOfAvailableCopies: new FormControl(null, Validators.required),
      isbn: new FormControl(null, Validators.required)
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    this.create.emit({
      form: this.createForm.value,
      image: this.imageFile
    });
  }

  closeForm() {
    this.close.emit();
  }

  handleFileInput(file) {
    this.imageFile = file;
  }

}
