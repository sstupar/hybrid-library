export enum HybridLibraryApiUrls {
  books = '/books',
  mostRentedBook = '/most-rented-book',
  overduereturns = '/overdue-returns',
  users = '/users'
}
