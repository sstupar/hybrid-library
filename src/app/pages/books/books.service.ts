import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {HybridLibraryApiUrls} from '../../api/hybrid-library-api-urls';
import {Observable} from 'rxjs';
import {BookModel} from '../../models/book.model';

@Injectable({providedIn: 'root'})
export class BooksService {
  apiUrl: string;

  constructor(
    private http: HttpClient
  ) {
    this.apiUrl = HybridLibraryApiUrls.books;
  }

  getAllBooks = (): Observable<BookModel[]> =>
    this.http.get<BookModel[]>(this.apiUrl);

  getBook = (id: number): Observable<BookModel> =>
    this.http.get<BookModel>(`${this.apiUrl}/${id}`);

  create = (data: BookModel): Observable<BookModel> =>
    this.http.post<BookModel>(this.apiUrl, data);

  update = (data: BookModel): Observable<BookModel> =>
    this.http.put<BookModel>(`${this.apiUrl}/${data.id}`, data);

  delete = (id) =>
    this.http.delete(`${this.apiUrl}/${id}`);

  rentBook = (id) =>
    this.http.post(`${this.apiUrl}/${id}/rent`, id);

  returnBook = (id) =>
    this.http.post(`${this.apiUrl}/${id}/return`, id);

  uploadImage = (file, id) => {
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'multipart/form-data');
    const uploadData = new FormData();
    uploadData.append('file', file);
    return this.http.put(`${this.apiUrl}/${id}/image`, uploadData, {headers});
  };
}
