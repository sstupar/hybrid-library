import {Component, OnInit} from '@angular/core';
import {BooksService} from './books.service';
import {Observable} from 'rxjs';
import {BookModel} from '../../models/book.model';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  books$: Observable<BookModel[]>;
  book: BookModel;
  create: Boolean = false;
  edit: Boolean = false;

  constructor(
    private booksService: BooksService
  ) {
  }

  ngOnInit() {
    this.getBooks();
  }

  getBooks() {
    this.books$ = this.booksService.getAllBooks();
  }

  createBook(data) {
      this.booksService.create(data.form)
        .subscribe((res) => {
          this.booksService.uploadImage(data.image, res.id).subscribe(() => {
            this.create = false;
          });
        });
  }

  updateBookCover(data) {
    this.booksService.uploadImage(data.image, data.id)
      .subscribe((res: BookModel) => {
      this.book = res;
    });
  }


  rentBook(e) {
    this.booksService.rentBook(e)
      .subscribe((res) =>
        console.log(res)
      );
  }

  editBook(id) {
    this.edit = true;
    this.booksService.getBook(id).subscribe((book) => {
      this.book = book;
    });
  }

  updateBook(data) {
    this.booksService.update(data)
      .subscribe(() => this.edit = false);
  }

  deleteBook(e) {
    this.booksService.delete(e)
      .subscribe(() =>
        this.getBooks()
      );
  }

}
