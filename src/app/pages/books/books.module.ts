import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BooksComponent} from './books.component';
import {NbButtonModule, NbInputModule} from '@nebular/theme';
import {BookComponent} from '../../components/book/book.component';
import {CreateBookComponent} from '../../components/create-book/create-book.component';
import {EditBookComponent} from '../../components/edit-book/edit-book.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    BooksComponent,
    BookComponent,
    CreateBookComponent,
    EditBookComponent
  ],
  imports: [
    CommonModule,
    NbButtonModule,
    NbInputModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: []
})
export class BooksModule { }
