import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {HybridLibraryApiUrls} from '../../api/hybrid-library-api-urls';

@Injectable({providedIn: 'root'})
export class DashboardService {

  constructor(
    private http: HttpClient
  ) {

  }

}
