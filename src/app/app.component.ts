import {Component} from '@angular/core';
import {NbMenuItem, NbSidebarService} from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'hybrid-library';
  items: NbMenuItem[] = [
    {
      title: 'Dashboard',
      link: '/dashboard',
      icon: 'nb-home'
    }, {
      title: 'Books',
      link: '/booklist',
      icon: 'nb-search'
    },
  ];

  constructor(private sidebarService: NbSidebarService) {

  }

  toggleSidebar() {
    this.sidebarService.toggle(true);
  }
}
