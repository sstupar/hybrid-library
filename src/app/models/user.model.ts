export interface UserModel {
  firstName:	string;
  id:	number;
  lastName:	string;
  role:	string;
  username:	string;
}
