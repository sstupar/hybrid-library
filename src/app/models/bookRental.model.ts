export interface BookRentalModel {
  book: string;
  rentDate: string;
  returnByDate: string;
  returnDate: string;
  status: string;
  user: string;
}
