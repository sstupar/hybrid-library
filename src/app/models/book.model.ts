export interface BookModel {
  author:	string;
  id?:	number;
  isbn:	string;
  numberOfAvailableCopies:	number;
  numberOfCopies:	number;
  publisher:	string;
  title:	string;
  coverImageHash: string;
}
